'''
Python Decorators – Key Takeaways. Decorators define reusable building blocks you can apply to a callable 
to modify its behavior without permanently modifying the callable itself. The @ syntax is just a shorthand
for calling the decorator on an input function.
'''

#for 1 type of function
#search for decorators wit arguments ########
import time
def memoize(func):
    cache = {}
    def inner(arg):
        start = time.time()
        if cache.get(arg):
            end = time.time()
            print('time taken using the cache is : '+ str(end - start))
            return cache.get(arg)
        result = func(arg)
        cache[arg] = result
        end = time.time()
        print('time taken to execute this function is : '+ str(end - start))
        return result
    return inner

import math
@memoize
def square_root(num):
    return math.sqrt(num)

print(square_root(100))

###################trail#####################


#what if we want to do it for any kind of function...
#we want to know the time taken for execuetion of that function, result of it and args of it

def generic_decorator(funct):
    def inner(*args , **kwargs):
        start_time = time.time()
        
        result = funct(*args , **kwargs)
        
        end_time = time.time()
        print(funct.__name__)
        print('time takem : '+str(end_time - start_time))
        return result
    return inner
@generic_decorator
def indy(*args , **kwargs):
    result = []
    result.append(args)
    result.append(kwargs)
    return result
        
print(indy(1 ,4 ,5 ,2, abc = 2 , good = 24))
print(indy(1 ,2))

#here we can take back-tracking algorithm in chess and wrap it with generic_decorator function just to get to know
#the time and its result before it is actually used for computaion in real case...



#lets take a function and wrap it with user level customization

def main_decorator(phrase):
    def decorator(function):
        def inner():
            print(phrase + ' start')
            function()
            print(phrase + ' end')
        return inner
    return decorator

@main_decorator('function')
def jones():
    return 'indiana jones is a great movie'

jones()

#now get a function from any other 

#decorators with arguements 2

import random

def power_of(exponent):
    def decorator(function):
        def inner():
            return function() ** exponent
        return inner
    return decorator


@power_of(2)
def random_odd():
    return random.choice([1 , 3 ,5 ,7 ,9])
print(random_odd())

#the memoizing decorator function using class implementation
'''
class Element:
    def __init__(self , function):
        self._memory = {}
        self._func = function
        
    def __call__(self , arg):
        start = time.time()
        if self._memory.get(arg):
            end = time.time()
            print('time taken through caching is : '+ str(start - end))
            return self._memory.get(arg)
        result = self._func(arg)
        self._memory[arg] = result
        end = time.time()
        print('time taken for computing is : '+ str(start - end))
        return result
    
    def memory(self):
        return self._memory
        
@Element
def fourthloop(a):
    result = 0
    for i in range(a):
        for j in range(a):
            for k in range(a):
                for l in range(a):
                    result = result + l+ i
    return result

fourthloop(4)
'''

##memoizatiopn wit limited stack using decorator parameters

from functools import lru_cache

@lru_cache(maxsize = 2)
def fourthloop(a):
    result = 0
    print('function started')
    for i in range(a):
        for j in range(a):
            for k in range(a):
                for l in range(a):
                    result = result + l+ i
    return result

fourthloop(4)
fourthloop(5)
fourthloop(6)
fourthloop(4)
            

        
        
        
        
        
        
        
        
        
        
        
        
        