import math
import time
from functools import lru_cache
#problem 1

def timer(function):
    def inner(x):
        result = []
        start = time.time()
        result.append(function(x))
        end = time.time()
        print(str(end - start))
        result.append(end - start)
        return result
    return inner

@timer
def sqrt(x):
    return math.sqrt(x)

@timer
def square(x):
    return x * x


def faster_function(sqrt , square , x):
    if sqrt(x)[1] > square(x)[1]:
        return square
    else:
        return sqrt
    
result = faster_function(sqrt , square , 4)
print(result(4))
##problem 2

#generic timer decorator

def generic_decorator(funct):
    def inner(*args , **kwargs):
        start = time.time()
        result = funct(*args , **kwargs)
        end = time.time()
        print(str(end - start))
        return result
    return inner

@generic_decorator
def add(x, y):
    return x + y

@generic_decorator
def add3(x, y, z):
    return x + y + z

@generic_decorator
def add_any(*args):
    return sum(args)

@generic_decorator
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total

x = add(4 , 5)
y = add3(7 , 8 , 9)
z = add_any(1 ,2 ,3 ,4, 5,6,7 , 8, 9, 10)
xyz = abs_add_any(1 , -9 , -10 , 3 , abs = True)


#problem 3 
#memoizing fib function 

def memoize(function):
    memory = {}
    def inner(arg):
        start = time.time()
        if memory.get(arg):
            end = time.time()
            print(str(end - start))
            return memory[arg]
        result = function(arg)
        memory[arg] = result
        end = time.time()
        print(str(end - start))
        return result
    return inner

@memoize
def fib(n):
    #print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)

fib(9)
fib(9)

#lru cache function

@lru_cache(maxsize = 10)
def factorial(n):
    print("Computing factorial({})".format(n))
    fact = 1
    for i in range(1, n + 1):
        fact *= i
    return fact

factorial(10)
factorial(10)

