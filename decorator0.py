
'''
-> IN python functions can be passed as arguments to other functions
-> These type of functions are called higher order functions.
-> in the same way, a function can return a function as its result…	

'''

def first(msg):
    print(msg)    

first("Hello")

second = first
second("Hello")

#functions can be passed as arguements

def top(a , b):
    return a>b

def bott(a , b):
    return a< b

def oper(func , a , b):
    return func(a , b)
oper(bott , 5 , 6)

#-> a function can be created inside the functioning returned

def is_called():
    def no_way():
        print('indiana')
    return no_way()
new = is_called
new()



'''
DECORATORS BASICS
'''
def decorate(funct):
    def inner():
        print('now im decorated...')
        return funct()
    return inner

def normal():
    return 'im a normal function... about to get decorated'

new_normal = (decorate)(normal)    
print(new_normal())
'''
-> We can see that the decorator function added some new functionality to the original function. 
This is similar to packing a gift. The decorator acts as a wrapper. The nature of the object that got decorated 
(actual gift inside) does not alter. But now, it looks pretty (since it got decorated).

'''

#-> Using decorators we can change the functionality of the existing function completely using decorators…

def training(funct):
    def inner():
        print('now im a dangerous cat... with metal clawwwwsssss...')
    return inner



@training    
def normal_cat():
    print('meowww... im a poor cat')


normal_cat()


# 2
def eagle():
    print('im a young eagle with sharp eyes')

def age_eagle(funct):
    def inner():
        funct()
        print('but time changes... now im an old eagle.')
    return inner

old_eagle = age_eagle(eagle)
eagle()
print('\n')
old_eagle()






